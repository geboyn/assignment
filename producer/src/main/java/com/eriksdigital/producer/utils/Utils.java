package com.eriksdigital.producer.utils;

import com.eriksdigital.producer.model.Order;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static String getIdsFromOrderList(List<Order> orders) {
        if (!orders.isEmpty()) {
            List<String> ids = new ArrayList<>();
            for (Order order : orders) {
                ids.add(order.getId().toString());
            }
            return String.join(",", ids);
        }
        return "";
    }
}