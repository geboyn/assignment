package com.eriksdigital.producer.kafka;


import com.eriksdigital.producer.notification.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;


public class Sender {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

    //topic used for comunicating with the consumer
    private String jsonTopic = "Kafka_Example";

    @Autowired
    private KafkaTemplate<String, Event> kafkaTemplate;

    public void send(Event event) {
        LOGGER.info("sending event='{}'", event.toString());
        kafkaTemplate.send(jsonTopic, event);
    }
}