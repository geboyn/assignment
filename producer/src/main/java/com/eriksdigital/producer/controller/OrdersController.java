package com.eriksdigital.producer.controller;

import com.eriksdigital.producer.model.Order;
import com.eriksdigital.producer.service.AuditService;
import com.eriksdigital.producer.service.OrdersService;
import com.eriksdigital.producer.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    private static final Logger logger = LoggerFactory.getLogger(OrdersController.class);

    private final OrdersService service;
    private final AuditService auditService;

    @Autowired
    public OrdersController(OrdersService service, AuditService auditService) {
        this.service = service;
        this.auditService = auditService;
    }

    //create a new order
    @PostMapping(value = "/create")
    public ResponseEntity<Order> create(@Valid @RequestBody final Order orders, HttpServletRequest request) throws IOException {
        Order order = service.createOrder(orders);
        logger.debug("Created new order with id {}", order.getId());
        auditService.sendToTopic(request, "Create order", order.getId().toString(), "ORDER_CREATED");
        logger.debug("Consumer notified about order {}", order.getId());
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    //get all orders (not including deleted ones)
    @GetMapping(value = "/all")
    public ResponseEntity<List<Order>> readAll(HttpServletRequest request) {
        List<Order> orders = service.retrieveAllOrders();
        logger.debug("Retrieved {} orders ", orders.size());
        auditService.sendToTopic(request, "Get all orders", Utils.getIdsFromOrderList(orders), "ORDERS_QUERIED");
        logger.debug("Consumer notified about orders request");
        return new ResponseEntity<>(orders, HttpStatus.OK);

    }

    //get all orders for a specific client
    @GetMapping(value = "/{clientName}")
    public ResponseEntity<List<Order>> findByClientName(@PathVariable final String clientName, HttpServletRequest request) {
        List<Order> orders = service.findByClientName(clientName);
        logger.debug("Retrieved all the orders for client ", clientName);
        auditService.sendToTopic(request, "Get orders for a client", Utils.getIdsFromOrderList(orders), "ORDERS_QUERIED");
        logger.debug("Consumer notified about orders request for client ", clientName);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity updateStatus(@RequestBody final String status, @PathVariable final Long id, HttpServletRequest request) {
        Optional<Order> orders = service.updateOrder(status, id);
        logger.debug("Updated order wtih id ", id);
        auditService.sendToTopic(request, "Update status for order", orders.map(order -> order.getId().toString()).orElse(""), "ORDER_STATUS_UPDATE");
        logger.debug("Consumer notified about order {} update with status {}", id, status);
        return new ResponseEntity(orders, HttpStatus.OK);

    }

    //delete an order by it's id. The row will not be deleted from the database. It will only be marked as deleted and ingored in other queries
    @PutMapping(value = "/delete/{id}")
    public ResponseEntity logicalDeleteById(@PathVariable final Long id, HttpServletRequest request) {
        service.removeOrderById(id);
        logger.debug("Removed order with id {}", id);
        auditService.sendToTopic(request, "Delete order ", id.toString(), "ORDER_DELETE");
        logger.debug("Consumer notified about order {} deletion", id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

}
