package com.eriksdigital.producer.repository;

import com.eriksdigital.producer.model.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public interface OrderJpaRepository extends CrudRepository<Order, Long> {

    @Query("SELECT o FROM Order o WHERE o.deleted = false")
    List<Order> findAllActive();

    @Query("SELECT o FROM Order o WHERE o.deleted = false AND o.clientName = :name")
    List<Order> findByClientName(@Param("name") String name);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Order SET deleted = true WHERE id = :id")
         void logicalDeleteById(@Param("id") Long id);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Order SET status = :status WHERE id = :id")
    void updateStatusById(@Param("id") Long id, @Param("status") String status);

    @Override
    @Query("SELECT o FROM Order o")
    List<Order> findAll();

}
