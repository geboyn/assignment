package com.eriksdigital.producer.service;

import com.eriksdigital.producer.exceptions.OrderNotFoundException;
import com.eriksdigital.producer.model.Order;
import com.eriksdigital.producer.repository.OrderJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {

    private static final Logger logger = LoggerFactory.getLogger(OrdersService.class);

    private final OrderJpaRepository ordersJpaRepository;

    @Autowired
    public OrdersService(OrderJpaRepository ordersJpaRepository) {
        this.ordersJpaRepository = ordersJpaRepository;
    }

    public List<Order> retrieveAllOrders() {
        return ordersJpaRepository.findAllActive();
    }

    public Order createOrder(Order order) {
        ordersJpaRepository.save(order);
        return order;
    }

    public Optional<Order> updateOrder(String status, Long id) {

        Optional<Order> oldOrder = ordersJpaRepository.findById(id);
        if (oldOrder.isPresent()) {
            logger.debug("Updating status for order {}", id);
            ordersJpaRepository.updateStatusById(id, status);
            logger.debug("Order {} has been updated with status {}", id, status);
            return ordersJpaRepository.findById(id);
        }
        logger.debug("Order {} has not been found!", id);
        throw new OrderNotFoundException("id :" + id);
    }

    public List<Order> findByClientName(String name) {
        List<Order> orders = ordersJpaRepository.findByClientName(name);
        if (!orders.isEmpty()) {
            return orders;
        }

        logger.debug("No orders found for client {}", name);
        return new ArrayList<>();
    }

    public void removeOrderById(Long id) {
        if (ordersJpaRepository.findById(id).isPresent()) {
            ordersJpaRepository.logicalDeleteById(id);
        } else {
            throw new OrderNotFoundException("id :" + id);
        }
    }
}
