package com.eriksdigital.producer.service;

import com.eriksdigital.producer.kafka.Sender;
import com.eriksdigital.producer.notification.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class AuditService {

    private final Sender sender;

    @Autowired
    public AuditService(Sender sender) {
        this.sender = sender;
    }

    private Event computeMessageForAudit(HttpServletRequest request, String description, String ids, String eventType) {
        Event event = new Event();
        event.setUser(request.getUserPrincipal().getName());
        event.setDescription(description);
        event.setReferencedIds(ids);
        event.setEventType(eventType);
        return event;
    }

    public void sendToTopic(HttpServletRequest request, String description, String ids, String eventType) {
        sender.send(computeMessageForAudit(request, description, ids, eventType));
    }
}
