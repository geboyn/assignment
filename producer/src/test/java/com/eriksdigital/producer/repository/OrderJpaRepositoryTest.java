package com.eriksdigital.producer.repository;

import com.eriksdigital.producer.model.Order;
import com.google.common.collect.Iterators;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Objects;


@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderJpaRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OrderJpaRepository orderJpaRepository;

    @Before
    public void setUp() {
        Order order = new Order();
        order.setClientName("Client");
        order.setPrice(new BigDecimal(1));
        entityManager.persist(order);
        entityManager.flush();
    }

    @Test
    public void testFindAllActive() {
        Iterable<Order> all = orderJpaRepository.findAll();
        assert Iterators.size(all.iterator()) != 0;

    }

    @Test
    public void testFindByClientName() {
        assert orderJpaRepository.findByClientName("Client").size() == 1;
        assert orderJpaRepository.findByClientName("OtherClient").size() == 0;

        entityManager.clear();
    }

    @Test
    public void logicalDeleteByIdTest() {
        orderJpaRepository.logicalDeleteById(orderJpaRepository.findAll().get(0).getId());

        assert orderJpaRepository.findAllActive().size() == 0;
        assert orderJpaRepository.findAll().size() == 1;

    }

    @Test
    public void testUpdateStatusById() {
        Long id = orderJpaRepository.findAll().get(0).getId();
        orderJpaRepository.updateStatusById(id, "baaaad");

        assert Objects.equals(orderJpaRepository.findById(orderJpaRepository.findAll().get(0).getId()).get().getStatus(), "baaaad");
    }

}
