package com.eriksdigital.producer.service;

import com.eriksdigital.producer.model.Order;
import com.eriksdigital.producer.repository.OrderJpaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class OrdersServiceTest {

    @Mock
    private OrderJpaRepository orderJpaRepository;


    @InjectMocks
    private OrdersService service;


    @Before
    public void setUp() throws Exception {
        Order order = new Order();
        order.setId(1L);
        order.setClientName("Test");



        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateOrder() throws Exception {

        Order order = new Order();
        order.setId(1L);
        order.setClientName("Test");

        when(orderJpaRepository.findById(1L)).thenReturn(Optional.of(order));
        assert service.createOrder(order).equals(order);

    }

    @Test
    public void testRetrieveAllOrders() throws Exception{

        assert service.retrieveAllOrders().isEmpty();

        Order order = new Order();
        order.setId(1L);
        order.setClientName("Test");
        List<Order> orders = new ArrayList<>();
        orders.add(order);

        when(orderJpaRepository.findAllActive()).thenReturn(orders);
        assert service.retrieveAllOrders().equals(orders);
    }


/*    @Test
    public void testUpdateOrder() throws Exception{
        Order order = new Order();
        order.setId(1L);
        order.setClientName("Test");

        when(orderJpaRepository.findById(1L)).thenReturn(Optional.of(order));
        service.updateOrder("RECEIVED_BY_CUSTOMER", 1L);

        System.out.println(service.findById(1L).get().getStatus());

        assert service.findById(1L).get().getStatus() != "NEW";
    }

    @Test
    public void testDeleteOrder() throws Exception{
        Order order = new Order();
        order.setId(1L);
        order.setClientName("Test");

        when(orderJpaRepository.findOrderById(1L)).thenReturn(order);
        service.removeOrderById(1L);

        System.out.println(service.findById(1L).get().getDeleted());
    }*/

    /*public List<Order> retrieveAllOrders(Pageable pageable, HttpServletRequest request) {
        List<Order> allOrders = ordersJpaRepository.findAllActive(pageable);

        List<String> ids = new ArrayList<>();
        for (int i = 0; i < allOrders.size(); i++) {
            ids.add(allOrders.get(i).getId().toString());
        }

        String idList = String.join(",", ids);

        Event event = computeMessageForAudit(request);
        event.setEventType("GET");
        event.setReferencedIds(idList);
        event.setDescription("Request for all orders");

        sendToTopic(event);
        return allOrders;
    }*/

}


    /*public Order createOrder(Order orders, HttpServletRequest request) {

        ordersJpaRepository.save(orders);
        Event event = computeMessageForAudit(request);
        event.setEventType("SAVE");
        event.setReferencedIds(orders.getId().toString());
        event.setDescription("Save order");
        sendToTopic(event);

        return orders;

    }*/
