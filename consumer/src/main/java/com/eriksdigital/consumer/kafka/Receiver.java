package com.eriksdigital.consumer.kafka;

import java.util.concurrent.CountDownLatch;

import com.eriksdigital.producer.notification.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * Class which consumes messages sent by the producer
 * */

public class Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

    private CountDownLatch latch = new CountDownLatch(1);

    public CountDownLatch getLatch() {
        return latch;
    }

    @KafkaListener(topics = "Kafka_Example")
    public void receive(Event event) {
        LOGGER.info("received event='{}'", event.toString());
        latch.countDown();
    }
}