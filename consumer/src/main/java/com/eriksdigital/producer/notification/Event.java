package com.eriksdigital.producer.notification;


/**
 * Same class as in the producer. Had to keep the same package name, because I was not able to
 * get over the trusted packages verification needed for serializing the message.
 * Didn't want to write my own serializer.
 * */
public class Event {

    private String referenceIds;
    private String user;
    private String eventType;
    private String description;

    public String getReferencedIds() {
        return referenceIds;
    }

    public void setReferencedIds(String referenceIds) {
        this.referenceIds = referenceIds;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Event{" +
                "referenceIds='" + referenceIds + '\'' +
                ", user='" + user + '\'' +
                ", eventType='" + eventType + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
