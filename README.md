# Order Management Service
This project is the home assignment for ERIKS Digital Java Backend Developer.

## Installation and usage

### Prerequisites
[java 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
[maven](https://maven.apache.org/download.cgi)
[kafka](https://kafka.apache.org/)
[zoookeper](https://zookeeper.apache.org/)
[Kafka & zookeeper (docker verison)](https://hub.docker.com/r/johnnypark/kafka-zookeeper/) - Optional
[Docker](https://www.docker.com/get-started) - Optional

### Build
There are two microservices: **producer** and **consumer**.
Running `mvn clean install` will build them and will generate the `jar` files which you will need to run.

### Run the app 
 - start the kafka and zookeper servers:
 `bin/zookeeper-server-start.sh config/zookeeper.properties`
 `bin/kafka-server-start.sh config/server.properties`
 **or**
` docker pull johnnypark/kafka-zookeeper`
`docker run -p 2181:2181 -p 9092:9092 -e ADVERTISED_HOST=127.0.0.1  johnnypark/kafka-zookeeper`
 - start the producer app
 `java -jar target/producer-0.0.1-SNAPSHOT.jar`
 
 - start the consumer app
 `java -jar target/consumer-0.0.1-SNAPSHOT.jar`
 - make some requests
 Please note the **hardcoded login** for the Basic Authentication
**user: test**
**pass: test**
 
 - send requests on http://localhost:8080/orders
 Request examples available at [localhost:8080/swagger-ui.html](localhost:8080/swagger-ui.html)
 
 - Check consumer output.

### Docker version
 I have tried to make the application run in a docker container, but I haven't tested it enough.
 I've added docker files on the **docker** branch.
 `mvn clean package`
 `mvn build -d spring-boot-producer .` 
 +
 `mvn clean package`
` mvn build -d spring-boot-consumer .`

`docker-compose -p dev -f docker-compose-test.yml up` (the file from the root of the project)
Also, in /etc/hosts, you should have 127.0.0.1       kafka-server entry.


If you choose to run the apps from Intellij for example, please open them separately, in two instances of the IDE.
The few tests I wrote will run at `mvn clean install`.

### Nice to have

 - better test coverage
 - all the config options migrated in the yml files
 - more complex model for orders (Order entity with OrderLine entity and ManyToOne mapping between them, customers table etc)
 - login from database
 - database in a docker container instead of in-memory database
